﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Enums;
using Agl.CodeChallange.Core.Models;
using Agl.CodeChallange.Core.Services;
using NSubstitute;
using NUnit.Framework;

namespace Agl.CodeChallange.Core.Tests.Services
{
    [TestFixture]
    public class PetServiceTests
    {
        private PetService _petService;

        [SetUp]
        public void Setup()
        {
            var mockedJsonParserService = Substitute.For<IJsonParserService>();
            var owners = new List<Person>
            {
                new Person
                {
                    Gender = Genders.Male,
                    Pets = new List<Pet>
                    {
                        new Pet {Name = "Garfield", Type = "cat"},
                        new Pet {Name = "Joth", Type = "cat"},
                        new Pet {Name = "MrDog", Type = "dog"}
                    }
                },
                new Person
                {
                    Gender = Genders.Female,
                    Pets = new List<Pet>
                    {
                        new Pet {Name = "Garfield", Type = "cat"},
                        new Pet {Name = "fishy", Type = "fish"}
                    }
                }
            };
            mockedJsonParserService.ParseJsonContentAsync(Arg.Any<string>()).Returns(Task.FromResult(owners.AsEnumerable()));

            _petService = new PetService(mockedJsonParserService);
        }

        [Test]
        public async Task GetListOfPetOwnershipsFromJsonUrlAsync_ShouldReturnSortedListOfcats_IfCatTypeRequested()
        {
            //Arrange
            const string url = "http://blah.com/test.json";

            //Act
            var petOwnerships = (await _petService.GetListOfPetOwnershipsFromJsonUrlAsync(url, PetTypes.Cat)).ToList();

            //Assert
            Assert.AreEqual(2, petOwnerships.Count);
            Assert.AreEqual(Genders.Male, petOwnerships[0].OwnerGender);
            Assert.AreEqual("Garfield", petOwnerships[0].PetNames.First());
            Assert.AreEqual("Joth", petOwnerships[0].PetNames.Last());
            Assert.AreEqual(Genders.Female, petOwnerships[1].OwnerGender);
            Assert.AreEqual("Garfield", petOwnerships[1].PetNames.Single());
        }
    }
}