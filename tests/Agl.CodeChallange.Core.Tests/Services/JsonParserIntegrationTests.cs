﻿

using System.Collections.Generic;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Exceptions;
using Agl.CodeChallange.Core.Models;
using Agl.CodeChallange.Core.Services;
using NUnit.Framework;

namespace Agl.CodeChallange.Core.Tests.Services
{
    [TestFixture]
    public class JsonParserIntegrationTests
    {
        private JsonParserService _jsonParserService;

        [SetUp]
        public void Setup()
        {
            var httpService = new HttpService();
            _jsonParserService = new JsonParserService(httpService);
        }

        [TestCase("http://agl-developer-test.azurewebsites.net/people.json")]
        public async Task ParseJsonContentAsync_ShouldReturnJsonObject_IfCorrectUrlPassed(string url)
        {
            //Arrange & Act
            var jsonObj = await _jsonParserService.ParseJsonContentAsync(url);

            //Assert
            Assert.IsInstanceOf<IEnumerable<Person>>(jsonObj);
        }

        [TestCase("")]
        [TestCase("http://invalidUrl")]
        public void ParseJsonContentAsync_ShouldThrownAnException_IfInvalidUrlPassed(string url)
        {
            //Arrange & Act
            Assert.ThrowsAsync<JsonContentNotFormattedWellException>(async () => 
                await _jsonParserService.ParseJsonContentAsync(url));
        }
    }
}
