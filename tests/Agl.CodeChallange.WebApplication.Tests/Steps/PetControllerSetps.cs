﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Agl.CodeChallange.Core.Enums;
using Agl.CodeChallange.Core.Models;
using Agl.CodeChallange.Core.Services;
using Agl.CodeChallange.WebApplication.Controllers;
using Agl.CodeChallange.WebApplication.Tests.Helpers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Agl.CodeChallange.WebApplication.Tests.Steps
{
    [Binding]
    public class PetControllerSteps
    {
        private ActionResult _actionResult;
        private PetController _petController;

        [Given(@"I have provided the remote json url")]
        public void GivenIHaveProvidedTheRemoteJsonUrl()
        {
            Setup();
        }


        [When(@"I call the pet endpoint")]
        public async Task WhenICallThePetEndpoint()
        {
            _actionResult = await _petController.PetList();
        }

        [Then(@"the result should a list of cat names group by their owner's gender")]
        public void ThenTheResultShouldAListOfCatNamesGroupByTheirOwnerSGender()
        {
            var models = ActionResultHelper.ModelFromActionResult<List<PetOwnership>>(_actionResult).ToList();

            Assert.AreEqual(Genders.Male, models[0].OwnerGender);
            Assert.AreEqual(4, models[0].PetNames.Count());
            var petnames = models[0].PetNames.ToList();
            Assert.AreEqual("Garfield", petnames[0]);
            Assert.AreEqual("Jim", petnames[1]);
            Assert.AreEqual("Max", petnames[2]);
            Assert.AreEqual("Tom", petnames[3]);

            Assert.AreEqual(Genders.Female, models[1].OwnerGender);
            Assert.AreEqual(3, models[1].PetNames.Count());
            petnames = models[1].PetNames.ToList();
            Assert.AreEqual("Garfield", petnames[0]);
            Assert.AreEqual("Tabby", petnames[1]);
            Assert.AreEqual("Simba", petnames[2]);
        }

        private void Setup()
        {
            var httpService = new HttpService();
            var jsonParserService = new JsonParserService(httpService);
            var petService = new PetService(jsonParserService);

            _petController = new PetController(petService);
        }
    }
}