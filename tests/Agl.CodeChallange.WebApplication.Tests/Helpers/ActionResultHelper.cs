﻿using System;
using System.Web.Mvc;

namespace Agl.CodeChallange.WebApplication.Tests.Helpers
{
    public static class ActionResultHelper
    {
        /// <summary>
        ///     Get the model or list of models from ActionResult
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="actionResult">The actionResult outcome of view with model</param>
        /// <returns>An object which represents model or mdoels from ActionResult</returns>
        public static T ModelFromActionResult<T>(ActionResult actionResult)
        {
            object model;
            if (actionResult.GetType() == typeof(ViewResult))
            {
                var viewResult = (ViewResult) actionResult;
                model = viewResult.Model;
            }
            else if (actionResult.GetType() == typeof(PartialViewResult))
            {
                var partialViewResult = (PartialViewResult) actionResult;
                model = partialViewResult.Model;
            }
            else
            {
                throw new InvalidOperationException(
                    $"Actionresult of type {actionResult.GetType()} is not supported by ModelFromResult extractor.");
            }

            var typedModel = (T) model;
            return typedModel;
        }
    }
}