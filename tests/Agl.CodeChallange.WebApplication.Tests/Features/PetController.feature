﻿Feature: PetController
	In order to parse animals Json file from remote url
	As a user
	I want to be able to view list of cat names based on Cat's owner gender

Scenario: I am a user who would like to get list of cat names based on owner's gender
	Given I have provided the remote json url
	When I call the pet endpoint
	Then the result should a list of cat names group by their owner's gender