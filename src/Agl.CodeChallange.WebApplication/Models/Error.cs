﻿namespace Agl.CodeChallange.WebApplication.Models
{
    public class Error
    {
        public Error(string title, string message)
        {
            Title = title;
            Message = message;
        }

        /// <summary>
        /// The error title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The error message
        /// </summary>
        public string Message { get; set; }
    }
}
