﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Agl.CodeChallange.Core.Enums;
using Agl.CodeChallange.Core.Models;
using Agl.CodeChallange.Core.Services;
using Agl.CodeChallange.WebApplication.Models;

namespace Agl.CodeChallange.WebApplication.Controllers
{
    public class PetController : Controller
    {
        //Todo: As an improvement, get the url via injection of setting interface
        private const string JsonUrl = "http://agl-developer-test.azurewebsites.net/people.json";

        private readonly IPetService _petService;
        
        public PetController(IPetService petService)
        {
            _petService = petService ?? throw new ArgumentNullException(nameof(PetService));
        }

        public async Task<ActionResult> PetList()
        {
            try
            {
                var petOwnerships = await _petService.GetListOfPetOwnershipsFromJsonUrlAsync(JsonUrl, PetTypes.Cat);
                return View(petOwnerships.ToList());
            }
            catch (Exception e)
            {
                return View("Error", new Error("Error Occured", e.Message));
            }
        }
    }
}