﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace Agl.CodeChallange.WebApplication
{
    public static class AutofacConfigurator
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule(new AglModule());

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}