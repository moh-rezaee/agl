﻿using Agl.CodeChallange.Core.Services;
using Autofac;

namespace Agl.CodeChallange.WebApplication
{
    public class AglModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HttpService>().As<IHttpService>().SingleInstance();

            builder.RegisterType<JsonParserService>().As<IJsonParserService>();
            builder.RegisterType<PetService>().As<IPetService>();

            base.Load(builder);
        }
    }
}