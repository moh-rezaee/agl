﻿using System;

namespace Agl.CodeChallange.Core.Exceptions
{
    public class HttpContentNotReadableException : Exception
    {
        public HttpContentNotReadableException(string message) : base(message)
        {
        }
    }
}