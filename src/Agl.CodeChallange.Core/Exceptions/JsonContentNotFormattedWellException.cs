﻿using System;

namespace Agl.CodeChallange.Core.Exceptions
{
    public class JsonContentNotFormattedWellException : Exception
    {
        public JsonContentNotFormattedWellException(string message) : base(message)
        {
        }
    }
}