﻿namespace Agl.CodeChallange.Core.Enums
{
    public enum PetTypes
    {
        Cat,
        Dog,
        Fish
    }
}