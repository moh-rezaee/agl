﻿using System.Collections.Generic;
using Agl.CodeChallange.Core.Enums;

namespace Agl.CodeChallange.Core.Models
{
    public class PetOwnership
    {
        /// <summary>
        ///     The pet's owner gender
        /// </summary>
        public Genders OwnerGender { get; set; }

        /// <summary>
        ///     The list of pets belong the the specific owner's gender
        /// </summary>
        public IEnumerable<string> PetNames { get; set; }
    }
}