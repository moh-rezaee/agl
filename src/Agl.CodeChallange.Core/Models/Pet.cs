﻿using Newtonsoft.Json;

namespace Agl.CodeChallange.Core.Models
{
    public class Pet
    {
        [JsonProperty(PropertyName = "name")] public string Name { get; set; }

        [JsonProperty(PropertyName = "type")] public string Type { get; set; }
    }
}