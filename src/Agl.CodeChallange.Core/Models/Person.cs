﻿using System.Collections.Generic;
using Agl.CodeChallange.Core.Enums;
using Newtonsoft.Json;

namespace Agl.CodeChallange.Core.Models
{
    public class Person
    {
        [JsonProperty(PropertyName = "name")] public string Name { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public Genders Gender { get; set; }

        [JsonProperty(PropertyName = "age")] public int Age { get; set; }

        [JsonProperty(PropertyName = "pets")] public List<Pet> Pets { get; set; }
    }
}