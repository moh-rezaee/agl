﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Enums;
using Agl.CodeChallange.Core.Models;

namespace Agl.CodeChallange.Core.Services
{
    public class PetService : IPetService
    {
        private readonly IJsonParserService _jsonParserService;

        public PetService(IJsonParserService jsonParserService)
        {
            _jsonParserService = jsonParserService ?? throw new ArgumentNullException(nameof(jsonParserService));
        }

        public async Task<IEnumerable<PetOwnership>> GetListOfPetOwnershipsFromJsonUrlAsync(string jsonUrl, PetTypes petType)
        {
            var owners = await _jsonParserService.ParseJsonContentAsync(jsonUrl);

            var petOwnerships = from owner in owners
                where owner.Pets != null
                group owner.Pets?.OrderBy(o => o.Name)
                        .Where(o => string.Equals(o.Type, petType.ToString(), StringComparison.CurrentCultureIgnoreCase))
                        .Select(o => o.Name)
                    by owner.Gender
                into ownershipGroup
                select new PetOwnership
                {
                    OwnerGender = ownershipGroup.Key,
                    PetNames = ownershipGroup.SelectMany(name => name).Distinct()
                };

            return petOwnerships;
        }
    }
}