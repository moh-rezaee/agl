﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Models;

namespace Agl.CodeChallange.Core.Services
{
    public interface IJsonParserService
    {
        /// <summary>
        /// Parses the content of the json.
        /// </summary>
        /// <param name="jsonUrl">The json URL.</param>
        /// <returns>List of owners</returns>
        Task<IEnumerable<Person>> ParseJsonContentAsync(string jsonUrl);
    }
}