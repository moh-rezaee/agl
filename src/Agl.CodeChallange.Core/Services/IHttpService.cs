﻿using System.Threading.Tasks;

namespace Agl.CodeChallange.Core.Services
{
    public interface IHttpService
    {
        /// <summary>
        ///     Gets the content.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>Url's content as string</returns>
        Task<string> GetContentAsync(string url);
    }
}