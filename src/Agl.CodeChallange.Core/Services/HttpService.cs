﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Exceptions;

namespace Agl.CodeChallange.Core.Services
{
    public class HttpService :IHttpService
    {
        public async Task<string> GetContentAsync(string url)
        {
            try
            {
                var response = await ReadContentFromUrl(url);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                throw new HttpContentNotReadableException($"Http content can not be read as string. Exception is: {e.Message}");
            }
        }

        private static async Task<HttpResponseMessage> ReadContentFromUrl(string url)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(url);
            return response;
        }
    }
}
