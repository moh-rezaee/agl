﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Exceptions;
using Agl.CodeChallange.Core.Models;
using Newtonsoft.Json;

namespace Agl.CodeChallange.Core.Services
{
    public class JsonParserService : IJsonParserService
    {
        private readonly IHttpService _httpService;

        public JsonParserService(IHttpService httpService)
        {
            _httpService = httpService ?? throw new ArgumentNullException(nameof(httpService));
        }

        public async Task<IEnumerable<Person>> ParseJsonContentAsync(string jsonUrl)
        {
            try
            {
                var jsonContent = await GetJsonContentFromUrlAsync(jsonUrl);
                return JsonConvert.DeserializeObject<List<Person>>(jsonContent);
            }
            catch (Exception e)
            {
                throw new JsonContentNotFormattedWellException($"Can not parse the json content. {e.Message}");
            }
        }

        private async Task<string> GetJsonContentFromUrlAsync(string jsonUrl)
        {
            return await _httpService.GetContentAsync(jsonUrl);
        }
    }
}