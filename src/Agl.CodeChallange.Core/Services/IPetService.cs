﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Agl.CodeChallange.Core.Enums;
using Agl.CodeChallange.Core.Models;

namespace Agl.CodeChallange.Core.Services
{
    public interface IPetService
    {
        /// <summary>
        /// Gets the list of pets from json URL.
        /// </summary>
        /// <param name="jsonUrl">The json URL.</param>
        /// <param name="petType">Type of the pet.</param>
        /// <returns>List of petOwnership</returns>
        Task<IEnumerable<PetOwnership>> GetListOfPetOwnershipsFromJsonUrlAsync(string jsonUrl, PetTypes petType);
    }
}
