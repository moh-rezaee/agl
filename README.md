#AGL-CodeChallange
AGL Code Challange Test Puzzle. This project built on ASP.net Web Application and C#. The project could be run on Visual Studio.

The project contains of two parts:

**Src** This section has the main source code

**Tests** This section has different projects: 

* Unit tests (Included TDD & Integration test) 
* BDD test

#Execution
How to execute project: just compile it from Visual Studio and it should return a HTML view with prased list

How to execute test The simplest approach is to open Visual Studio => Test => Windows => Test Explorer. Once the explorer is open you should be able to see all tests and could run it from there.



